//Some code borrowed from Raining Chain
//https://www.youtube.com/channel/UC8Yp-YagXZ4C5vOduEhcjRw

//I also borrowed code the first Snake game we did in class

var ctx = document.getElementById("ctx").getContext("2d");
ctx.font = '30px Century Gothic';
 
var canvasHeight = 500;
var canvasWidth = 500;
 
var player = {
        x:50,
        y:450,
        width: 20,
        height: 20,
        health: 50,
        color: 'blue',
        pressingDown: false,
        pressingUp: false,
        pressingLeft: false,
        pressingRight: false
};

var nextLevelDoor = {
    x: 450,
    y: 50,
    width: 18,
    height: 45,
    color: 'brown'
};
 
var monsterList = {};
var treasureList = {};
var lavaPool = {};

Monster = function(id, x, y, spdX, spdY, width, height, color)
{
    var monster1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        width: width,
        height: height,
        id: id,
        color: color
                    };
    monsterList[id] = monster1;
}

Treasure = function(id, x, y, spdX, spdY, width, height)
{
    var treasure1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        name:'E',
        id: id,
        width: width,
        height: height,
        color: 'yellow'
                    };
    treasureList[id] = treasure1;
}

LavaArea = function(id, x, y, spdX, spdY, width, height)
{
    var lava1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        name:'E',
        id: id,
        width: width,
        height: height,
        color: 'red'
                    };
    lavaPool[id] = lava1;
}

//

document.onkeydown = function(event)
{
    if(event.keyCode === 68)
    {
        player.pressingRight = true;
    }
    
    else if(event.keyCode === 83)
    {
        player.pressingDown = true;
    }
    
    else if(event.keyCode === 65)
    {
        player.pressingLeft = true;
    }
    
    else if(event.keyCode === 87)
    {
        player.pressingUp = true;
    }
        
}

document.onkeyup = function(event)
{
    if(event.keyCode === 68)
    {
        player.pressingRight = false;
    }
    
    else if(event.keyCode === 83)
    {
        player.pressingDown = false;
    }
    
    else if(event.keyCode === 65)
    {
        player.pressingLeft = false;
    }
    
    else if(event.keyCode === 87)
    {
        player.pressingUp = false;
    }
}

updatePlayerPosition = function()
{
    if(player.pressingRight)
    {
        player.x += 8;
    }
    
    if(player.pressingLeft)
    {
        player.x -= 8;
    }
    
    if(player.pressingDown)
    {
        player.y += 8;
    }
    
    if(player.pressingUp)
    {
        player.y -= 8;
    }
}
 
updateEntity = function(shape)
{
    updateEntityPosition(shape);
    drawEntity(shape);
}

updateEntityPosition = function(shape)
{
    shape.x += shape.spdX;

    if(shape.x < 300 || shape.x > 400)
    {
        shape.spdX = -shape.spdX;
    }
}

drawEntity = function(shape)
{
    ctx.save();
    ctx.fillStyle = shape.color;
    ctx.fillRect(shape.x-shape.width/2, shape.y-shape.width/2, shape.width, shape.height);
    ctx.restore();
}

update = function(){
        ctx.clearRect(0,0, canvasWidth, canvasHeight);
        
        for(var i in monsterList)
        {
            updateEntity(monsterList[i]);
        }
        
        for(var i in lavaPool)
        {
            updateEntity(lavaPool[i]);
        }
        
        for(var i in treasureList)
        {
            updateEntity(treasureList[i]);
        }
        
        
        
        
        updatePlayerPosition();
        drawEntity(player);
        drawEntity(nextLevelDoor);
       
}

Monster('M1', 310, 85, 4, 15, 20, 20, 'purple');

Treasure('T1', 250, 250, 0, 0, 10, 10);

LavaArea('L2', 90, 120, 0, 0, 100, 100);
LavaArea('L3', 90, 260, 0, 0, 100, 100);

setInterval(update,16.6);
//Some code borrowed from Raining Chain
//https://www.youtube.com/channel/UC8Yp-YagXZ4C5vOduEhcjRw
//
//I also borrowed code the first Snake game we did in class

var ctx = document.getElementById("ctx").getContext("2d");
ctx.font = '30px Century Gothic';

//image object that stores the sprites
var Img = {};
Img.player_sprite = new Image();
Img.player_sprite.src = "images/knight_player.png";
Img.monster_sprite = new Image();
Img.monster_sprite.src = "images/ghost_2.png";
Img.lava_sprite = new Image();
Img.lava_sprite.src = "images/lava.png";
 
var canvasHeight = 500;
var canvasWidth = 500;
 
var player = {
        x:50,
        y:450,
        width: 30,
        height: 35,
        health: 50,
        img: Img.player_sprite,
        pressingDown: false,
        pressingUp: false,
        pressingLeft: false,
        pressingRight: false
};

var nextLevelDoor = {
    x: 450,
    y: 50,
    width: 18,
    height: 45,
    img: Img.player_sprite
};
 
var monsterList = {};
var treasureList = {};
var lavaPool = {};
var level = 0;
var score = 0;

Monster = function(id, x, y, spdX, spdY, width, height)
{
    var monster1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        width: width,
        height: height,
        id: id,
        img: Img.monster_sprite
        
                    };
    monsterList[id] = monster1;
}

Treasure = function(id, x, y, spdX, spdY, width, height)
{
    var treasure1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        name:'E',
        id: id,
        width: width,
        height: height,
        img: Img.monster_sprite
                    };
    treasureList[id] = treasure1;
}

LavaArea = function(id, x, y, spdX, spdY, width, height)
{
    var lava1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        name:'E',
        id: id,
        width: width,
        height: height,
        img: Img.lava_sprite
                    };
    lavaPool[id] = lava1;
}

document.onkeydown = function(event)
{
    if(event.keyCode === 68)
    {
        player.pressingRight = true;
    }
    
    else if(event.keyCode === 83)
    {
        player.pressingDown = true;
    }
    
    else if(event.keyCode === 65)
    {
        player.pressingLeft = true;
    }
    
    else if(event.keyCode === 87)
    {
        player.pressingUp = true;
    }
        
}

document.onkeyup = function(event)
{
    if(event.keyCode === 68)
    {
        player.pressingRight = false;
    }
    
    else if(event.keyCode === 83)
    {
        player.pressingDown = false;
    }
    
    else if(event.keyCode === 65)
    {
        player.pressingLeft = false;
    }
    
    else if(event.keyCode === 87)
    {
        player.pressingUp = false;
    }
}

updatePlayerPosition = function()
{
    if(player.pressingRight)
    {
        player.x += 4;
    }
    
    if(player.pressingLeft)
    {
        player.x -= 4;
    }
    
    if(player.pressingDown)
    {
        player.y += 4;
    }
    
    if(player.pressingUp)
    {
        player.y -= 4;
    }
}
 
updateEntity = function(shape)
{
    updateEntityPosition(shape);
    drawEntity(shape);
}

updateEntityPosition = function(shape)
{
    
    if(shape.id === 'M1')
    {
        if(level === 0)
        {
            shape.x += shape.spdX;
        
            if(shape.x < 300 || shape.x > 400)
            {
                shape.spdX = -shape.spdX;
            }
        }
        
        if(level === 1)
        {
            shape.y += shape.spdY;
        
            if(shape.y < 200 || shape.y > 400)
            {
                shape.spdY = -shape.spdY;
            }
        }
        
    }
    
    if(shape.id === 'M2')
    {
        if(level === 1)
        {
            shape.x += shape.spdX;
            
            if(shape.x < 40 || shape.x > 450)
            {
                shape.spdX = -shape.spdX;
            }
        }
    }
    
}

drawEntity = function(shape)
{
    ctx.save();
    var x = shape.x-shape.width/2;
    var y = shape.y-shape.width/2;
    
    //draws the image to correct proportions of width and height of the entity
    ctx.drawImage(shape.img, 0, 0, shape.img.width, shape.img.height,
            x, y, shape.width, shape.height);
    ctx.restore();
}

getDistanceBetweenPlayerandMonster = function(o1, o2)
{
    var vx = o1.x - o2.x;
    var vy = o1.y - o2.y;
    
    return Math.sqrt(vx * vx + vy * vy);
}

testCollision = function(o1, o2)
{
    var rec1 = {
        x : o1.x - o1.width/2,
        y : o1.y - o1.height/2,
        width: o1.width,
        height: o1.height
    }
    
    var rec2 = {
        x : o2.x - o2.width/2,
        y : o2.y - o2.height/2,
        width : o2.width,
        height : o2.height
    }
    
    return testCollisionForRectangles(rec1, rec2);
}

testCollisionForRectangles = function(rec1, rec2)
{
    return  rec1.x <= rec2.x + rec2.width
        &&  rec2.x <= rec1.x + rec1.width
        &&  rec1.y <= rec2.y + rec2.height
        &&  rec2.y <= rec1.y + rec1.height;
}

update = function(){
        ctx.clearRect(0,0, canvasWidth, canvasHeight);
        
        for(var i in monsterList)
        {
            updateEntity(monsterList[i]);
            
            var collision = testCollision(player, monsterList[i]);
            if(collision)
            {
                startGame();  //resets the game
            }
        }
        
        for(var i in lavaPool)
        {
            updateEntity(lavaPool[i]);
            
            var collision = testCollision(player, lavaPool[i]);
            if(collision)
            {
                startGame(); //resets the game
            }
        }
        
        for(var i in treasureList)
        {
            updateEntity(treasureList[i]);
            
            var collision = testCollision(player, treasureList[i]);
            if(collision)
            {
                score += 1;
                delete treasureList[i];
            }
        }
        
        var goal = testCollision(player, nextLevelDoor);
        if(goal)
        {
            if(level === 1)
            {
                startGame(); //resets the game
            }
            
            level += 1;
            startLevel2();
        }
        
        
        
        updatePlayerPosition();
        drawEntity(player);
        drawEntity(nextLevelDoor);
        ctx.fillText("Score: " + score, 0, 30);
       
}

startGame = function()
{
    player.x = 50;
    player.y = 450;
    score = 0;
    level = 0;
    monsterList = {};
    Monster('M1', 310, 85, 2, 15, 30, 30);
    Treasure('T1', 250, 250, 0, 0, 10, 10);
    Treasure('T2', 270, 250, 0, 0, 10, 10);
    Treasure('T3', 290, 250, 0, 0, 10, 10);

    LavaArea('L1', 110, 180, 0, 0, 120, 120);
    LavaArea('L2', 90, 340, 0, 0, 100, 100);
    LavaArea('L3', 420, 340, 0, 0, 75, 75);
    LavaArea('L4', 360, 300, 0, 0, 75, 75);
}

startLevel2 = function()
{
    ctx.clearRect(0,0, canvasWidth, canvasHeight);
    player.x = 50;
    player.y = 450;
    monsterList = {};
    
    
    Monster('M1', 310, 200, 4, 4, 30, 30);
    Monster('M2', 40, 60, 4, 4, 30, 30);
    
    Treasure('T1', 90, 285, 0, 0, 10, 10);
    Treasure('T2', 250, 270, 0, 0, 10, 10);
    Treasure('T3', 250, 290, 0, 0, 10, 10);
    
    LavaArea('L1', 230, 200, 0, 0, 85, 85);
    LavaArea('L2', 90, 340, 0, 0, 85, 85);
    LavaArea('L3', 145, 300, 0, 0, 85, 85);
    LavaArea('L4', 180, 240, 0, 0, 85, 85);
    
}

startGame();

//60fps
setInterval(update,16.6);
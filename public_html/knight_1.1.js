//Some code borrowed from RainingChain from Youtube.com
//https://www.youtube.com/channel/UC8Yp-YagXZ4C5vOduEhcjRw

//I also borrowed code the first Snake game we did in class

var ctx = document.getElementById("ctx").getContext("2d");
ctx.font = '30px Century Gothic';
 
var canvasHeight = 500;
var canvasWidth = 500;
 
var player = {
        x:50,
        y:450,
        width: 20,
        height: 20,
        health: 50,
        color: 'blue' 
};
 
var monsterList = {};
var treasureList = {};

//monster object
Monster = function(id, x, y, spdX, spdY, width, height, color)
{
    var monster1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        width: width,
        height: height,
        id: id,
        color: color
                    };
    monsterList[id] = monster1;
}

//treasure object
Treasure = function(id, x, y, spdX, spdY, width, height)
{
    var treasure1 = {
        x:x,
        spdX:spdX,
        y:y,
        spdY:spdY,
        name:'E',
        id: id,
        width: width,
        height: height,
        color: 'yellow'
                    };
    treasureList[id] = treasure1;
}
 
updateEntity = function(shape)
{
    updateEntityPosition(shape);
    drawEntity(shape);
}

//moves the shape by adding 4 to the spdX var while in the update function
updateEntityPosition = function(shape)
{
    shape.x += shape.spdX;

    if(shape.x < 300 || shape.x > 400)
    {
        shape.spdX = -shape.spdX;
    }
}

drawEntity = function(shape)
{
    ctx.save();
    ctx.fillStyle = shape.color;
    ctx.fillRect(shape.x-shape.width/2, shape.y-shape.width/2, shape.width, shape.height);
    ctx.restore();
}

update = function(){
        //clear the canvas
        ctx.clearRect(0,0, canvasWidth, canvasHeight);
        
        //looping through the monster list
        for(var i in monsterList)
        {
            updateEntity(monsterList[i]);
        }
        
        
        //looping through the treasure list
        for(var i in treasureList)
        {
            updateEntity(treasureList[i]);
        }
        
        drawEntity(player);
       
}

//passing values into Monster and treasure objects
Monster('M1', 310, 85, 4, 15, 20, 20, 'purple');
Treasure('T1', 250, 250, 0, 0, 10, 10); 

//60 fps
setInterval(update,16.6);
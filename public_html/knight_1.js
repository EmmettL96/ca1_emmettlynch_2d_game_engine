/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function()
{
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
})();

var canvas = document.getElementById("canvas"),
    ctx = canvas.getContext("2d"),
    width = 500,
    height = 200,
    player = {
        x : width/2,
        y : height - 15,
        width : 5,
        height : 5,
        speed : 3,
        velX : 0,
        velY : 0,
        jumping: false,
        grounded: false
        },
        keys = [],
        fpsElement = document.getElementById('fps');
        friction = 0.8,
        gravity = 0.3;
        
        
var boxes = [];
//Time
var lastAnimationFrameTime = 0,
    lastFpsUpdateTime = 0,
    fps = 60;

//images
var background = new Image();
var character = new Image();


//scrolling background
var STARTING_BACKGROUND_OFFSET = 0;
var STARTING_BACKGROUND_VELOCITY = 25;

//translation offsets
var backgroundOffset = STARTING_BACKGROUND_OFFSET;
var backgroundVelocity = STARTING_BACKGROUND_VELOCITY;



canvas.width = width;
canvas.height = height;

function update()
{
//check keys
if(keys[38] || keys[32] || keys[87])
{
    //up arrow or space
    if(!player.jumping && player.grounded)
    {
        player.jumping = true;
        player.grounded = false;
        player.velY = -player.speed*2;
    }
}

if(keys[39] || keys[68])
{
    //right arrow
    if(player.velX < player.speed)
    {
        player.velX++;
    }
}

if(keys[37] || keys[65])
{
    //left arrow
    if(player.velX > -player.speed)
    {
        player.velX--;
    }
}

player.velX *= friction;
player.velY += gravity;

ctx.clearRect(0,0, width, height);
ctx.fillStyle = "black";
ctx.beginPath();

player.grounded = false;
    
    for(var i = 0; i < boxes.length; i++)
    {
        ctx.rect(boxes[i].x, boxes[i].y, boxes[i].width, boxes[i].height);
        var dir = checkCollision(player, boxes[i]);
        
        if(dir === "l" || dir === "r")
        {
            player.velX = 0;
            player.jumping = false;
        }
        
        else if(dir === "b")
        {
            player.grounded = true;
            player.jumping = false;
        }
        
        else if(dir === "t")
        {
            player.velY *= -1;
        }
    }
    
    if(player.grounded)
    {
        player.velY = 0;
    }
    
    player.x += player.velX;
    player.y += player.velY;
    
    ctx.fill();
    ctx.fillStyle = "green";
    ctx.fillRect(player.x, player.y, player.width, player.height);
    
    initializeImages();
    requestAnimationFrame(update);
    window.requestAnimationFrame(animate);
}

function animate(now)
{
    fps = calculateFps(now);
    //draw(now);
    drawBoxes();
    lastAnimationFrameTime = now;
    fpsElement.innerHTML = fps.toFixed(0) + 'fps';
    
    return fps;
}

function calculateFps(now)
{
    var fps = 1/(now - lastAnimationFrameTime) * 1000;
    if(now - lastFpsUpdateTime > 1000)
    {
        lastFpsUpdateTime = now;
        fpsElement.innerHTML = fps.toFixed(0) + ' fps';
    }
    
    return fps;
}

function initializeImages()
{
    background.src = 'images/background.png';
    character.src = 'images/char.png';
}

function draw(now)
{
    setOffsets(now);
    drawBackground();
}

function drawBackground()
{
    ctx.translate(-backgroundOffset, 0);
    ctx.drawImage(background, 0, 0);
    ctx.drawImage(background, background.width, 0);
    ctx.translate(backgroundOffset, 0);
}

function setBackgroundOffset(now)
{
    backgroundOffset += backgroundVelocity*(now - lastAnimationFrameTime)/1000;
    
    if(backgroundOffset < 0 || backgroundOffset > background.width)
    {
        backgroundOffset = 0;
    }
}

function setOffsets(now)
{
    setBackgroundOffset(now);
}

function drawBoxes()
{
    //left wall
    boxes.push({
        x : 0,
        y : 0,
        width : 10,
        height: height
    });

    //ground level
    boxes.push({
        x : 0,
        y : height - 2,
        width: width,
        height: 50
    });

    //right wall
    boxes.push({
        x: width - 10,
        y: 0,
        width: 50,
        height: height
    });

    boxes.push({
        x: 120,
        y: 130,
        width: 90,
        height: 10
    });

    boxes.push({
        x: 230,
        y: 75,
        width: 90,
        height: 10
    });

    boxes.push({
        x: width/2,
        y: 160,
        width: 90,
        height: 10
    });

    boxes.push({
        x: 440,
        y: 110,
        width: 40,
        height: 40
    });
}

function checkCollision(shapeA, shapeB)
{
    //get the vectors to check against
    var vX = (shapeA.x + (shapeA.width / 2)) - (shapeB.x + (shapeB.width / 2)),
        vY = (shapeA.y + (shapeA.height / 2)) - (shapeB.y + (shapeB.height / 2)),
        //add the half widths and the half heights of the objects
        hWidths = (shapeA.width / 2 ) + (shapeB.width / 2),
        hHeights = (shapeA.height / 2) + (shapeB.height / 2),
        colDirection = null;

    if(Math.abs(vX) < hWidths && Math.abs(vY) < hHeights)
    {
        var oX = hWidths - Math.abs(vX),
            oY = hHeights - Math.abs(vY);

        if(oX >= oY)
        {
            if(vY > 0)
            {
                colDirection = "t";
                shapeA.y += oY;
            }
            else
            {
                colDirection = "b";
                shapeA.y -= oY;
            }
        }
        else
        {
            if(vX > 0)
            {
                colDirection = "l";
                shapeA.x += oX;
            }
            else
            {
                colDirection = "r";
                shapeA.x -= oX;
            }
        }
    
    
    }
    return colDirection;
}

document.body.addEventListener("keydown", function(e)
{
    keys[e.keyCode] = true;
});

document.body.addEventListener("keyup", function(e)
{
    keys[e.keyCode] = false;
});

window.addEventListener("load", function(){
  update();
});